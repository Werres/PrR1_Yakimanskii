﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ПрР1_Якиманский
{
     public class Matrix
    {
        public Matrix(int x, int y, float[,] mas, int count)
        {
            Console.WriteLine("Заполни матрицу");

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    Console.Write("mas[" + i + "," + j + "]: ");
                    mas[i, j] = float.Parse(Console.ReadLine());
                }
            }
            Console.WriteLine();

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    mas[i, j] = (float)Math.Sin(i + j / 2);
                    if (mas[i, j] > 0) count += 1;
                    Console.Write(" [" + i + "," + j + "]: " + mas[i, j] + "\t");
                }
                Console.WriteLine();
            }
            Console.Write("Ответ: число положительных элементов = " + count);
            Console.ReadKey();
        }
    }
}
